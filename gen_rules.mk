# Copyright (C) Quark Security Inc 2014

_orig_build_number := $(strip $(BUILD_NUMBER))
_fixed_build_number := $(subst .,_,$(strip $(BUILD_NUMBER)))

ifneq ($(_fixed_build_number),)
_variantfullname := $(VARIANT_NAME)_$(_fixed_build_number)
else
_variantfullname := $(VARIANT_NAME)
endif
_variantdir := $(outdir)/$(_variantfullname)
_poltar := $(outdir)/$(_variantfullname).policy.tgz

_poldir := $(_variantdir)/policy
_tmpdir := $(_variantdir)/tmp
_sepol_conf := $(_tmpdir)/sepolicy.conf
_sepol := $(_variantdir)/sepolicy
_fc := $(_variantdir)/file_contexts
_seapp := $(_variantdir)/seapp_contexts
_prop := $(_variantdir)/property_contexts
_mac := $(_variantdir)/mac_permissions.xml

ALL_POLICIES := $(ALL_POLICIES) $(_variantfullname)
DEPS := $(DEPS) $(lastword $(MAKEFILE_LIST))

all: $(_variantfullname)
clean: $(_variantfullname)-clean
bare: $(_variantfullname)-bare

.PHONY: bare clean $(_variantfullname) $(_variantfullname)-clean $(_variantfullname)-bare


$(_variantfullname)-clean: __poldir := $(_poldir)
$(_variantfullname)-clean:
	$(hide) rm -rf $(__poldir)

$(_variantfullname)-bare: __variantdir := $(_variantdir)
$(_variantfullname)-bare: $(_variantfullname)-clean
	$(hide) rm -rf $(__variantdir)

$(_variantfullname): $(_sepol) $(_fc) $(_seapp) $(_prop) $(_mac) $(_poltar)

$(_poldir): $(shell find $(LOCAL_PATH) -type f ! -regex \.git.*)
	@mkdir -p $@

$(_poltar): __variantdir := $(_variantdir)
$(_poltar): $(_sepol) $(_fc) $(_seapp) $(_prop) $(_mac)
	$(hide) tar -C $(__variantdir) -czf $(__variantdir)/$(notdir $@) $(foreach f,$^,$(notdir $(f)))
	$(hide) mv $(__variantdir)/$(notdir $@) $(outdir) 

$(_sepol_conf): __POLICY_DIRS := $(POLICY_DIRS)
$(_sepol_conf): __poldir := $(_poldir)
$(_sepol_conf): __tmpdir := $(_tmpdir)
$(_sepol_conf): $(_poldir) $(foreach _dir,$(POLICY_DIRS),$(shell find $(_dir) -type f)) $(DEPS)
	$(hide) rm -rf $(__poldir)
	@mkdir -p $(__poldir)
	@mkdir -p $(dir $@)
	$(hide) $(foreach _dir,$(__POLICY_DIRS),\
		$(foreach _subdir,$(_dir)/base/ $(_dir)/replace/, test -d $(_subdir) && find $(_subdir) -type f -execdir cp {} $(__poldir) \; || true ;)\
		test -d $(_dir)/union && find $(_dir)/union -type f ! -name mac_permissions.xml -exec  sh -c 'cat "{}" >> $(__poldir)/`basename "{}"`' \; || true ; \
		test -d $(_dir)/remove && find $(_dir)/remove -type f -exec  sh -c 'rm -f $(__poldir)/`basename "{}"`' \; || true ; )
	$(hide) cd $(__poldir); m4 -D mls_num_sens=$(MLS_SENS) -D mls_num_cats=$(MLS_CATS) -s $(pol_files) > $@.attr
	@# I dislike approach, as it means the line numbers reported by the compiler error messages might be mis-leading.
	@# TODO: come up with something better.
	$(hide) grep -e '^[\s]*typeattribute' $@.attr > $(__tmpdir)/typeattrs
	$(hide) sed  '/[\s]*typeattribute.*$$/d' $@.attr > $@.noattr
	$(hide) sed  -e '/^#line 1 "roles"$$/r $(__tmpdir)/typeattrs' $@.noattr > $@
ifneq ($(DONTAUDIT),)
	$(hide) cd $(__poldir); sed -i -e '/dontaudit/d' $@
endif

$(_sepol): $(_sepol_conf) $(checkpol)
	@mkdir -p $(dir $@)
	$(hide) $(checkpol) -M -c $(POLICYVERS) -o $@ $<

$(_poldir)/file_contexts $(_poldir)/seapp_contexts $(_poldir)/property_contexts $(_poldir)/keys.conf: $(_sepol)

# TODO: we might not need the m4 bits in the recipes below
$(_fc): __sepol := $(_sepol)
$(_fc): $(_poldir)/file_contexts $(_sepol) $(checkfc)
	@mkdir -p $(dir $@)
	$(hide) m4 -s $< > $@
ifndef DISABLE_CHECKS
	$(hide) $(checkfc) $(__sepol) $@
endif

$(_prop): __sepol := $(_sepol)
$(_prop): $(_poldir)/property_contexts $(_sepol) $(checkfc)
	@mkdir -p $(dir $@)
	$(hide) m4 -s $< > $@
ifndef DISABLE_CHECKS
	$(hide) $(checkfc) -p $(__sepol) $@
endif

$(_seapp): __sepol := $(_sepol)
$(_seapp): __tmpdir := $(_tmpdir)
$(_seapp): $(_poldir)/seapp_contexts $(_sepol) $(checkseapp)
	@mkdir -p $(dir $@)
	@mkdir -p $(__tmpdir)
	$(hide) m4 -s $< > $(__tmpdir)/seapp.tmp
ifndef DISABLE_CHECKS
	$(hide) $(checkseapp) -p $(__sepol) -o $@ $(__tmpdir)/seapp.tmp || ( rm $@; exit 1 )
else
	$(hide) cp $(__tmpdir)/seapp.tmp $@
endif

_all_mac_perms :=  $(foreach _dir,$(POLICY_DIRS), $(shell test -f $(_dir)/union/mac_permissions.xml && echo $(_dir)/union/mac_permissions.xml))
_all_mac_perms := $(shell test -f $(_poldir)/mac_permissions.xml && echo $(_poldir)/mac_permissions.xml)
$(_mac): __POLICY_DIRS := $(POLICY_DIRS)
$(_mac): __all_mac_perms := $(_all_mac_perms)
$(_mac): __poldir := $(_poldir)
$(_mac): $(_poldir)/keys.conf 
	@mkdir -p $(dir $@)
	$(hide) $(insertkeys) -t user -c $(CURDIR) $< -o $@ $(__all_mac_perms) $(shell test -f $(__poldir)/mac_permissions.xml && echo $(__poldir)/mac_permissions.xml) \
		$(foreach _dir,$(POLICY_DIRS), $(shell test -f $(_dir)/union/mac_permissions.xml && echo $(_dir)/union/mac_permissions.xml))
		

