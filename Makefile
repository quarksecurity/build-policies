# Copyright (C) Quark Security Inc, 2014
QUIET := n

include build/core.mk

help:
	@echo "Available policy build targets:"
	@for p in $(ALL_POLICIES); do echo " - $$p"; done
	@echo "Available policy clean targets:"
	@for p in $(ALL_POLICIES); do echo " - $$p-clean"; done
	@echo "Available policy bare targets:"
	@for p in $(ALL_POLICIES); do echo " - $$p-bare"; done
	@echo "Additionally, there are these targets available:"
	@echo " - all (build all policies)"
	@echo " - clean (clean all policies)"
	@echo " - bare (bare all policies)"

bare:
	$(hide) rm -rf out/

.PHONY: help bare

