# Copyright (C) Quark Security Inc 2014

VARIANT_NAME := $(strip $(VARIANT_NAME))
BUILD_NUMBER := $(strip $(BUILD_NUMBER))
$(if POLICY_DIRS,,$(error ERROR: POLICY_DIRS is not defined))
$(if VARIANT_NAME,,$(error ERROR: VARIANT_NAME is not defined))

include build/gen_rules.mk
include $(wildcard build_addons/*.mk)
DEPS := $(DEPS) $(shell test -d build_addons && find build_addons -type f)

VARIANT_NAME :=
BUILD_NUMBER :=
_fixed_build_number :=
_orig_build_number :=
_variantfullname :=
_variantdir :=
_poldir :=
_tmpdir :=
_sepol_conf :=
_sepol :=
_fc :=
_seapp :=
_prop :=
_all_mac_perms :=

# this can be used when AOSP's checkseapp fails due to vendor-specific format changes
DISABLE_CHECKS :=

