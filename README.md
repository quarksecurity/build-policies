# Introduction
This is a build system for generating multiple SE for Android policies with
varying configurations with a single command.  Each different configuration
will be referred to as a "variant".  One variant may have cellular access
enabled, while another variant may disable cellular but enable WiFi.

## Checking out the repos.

We use Googles 'repo' tool to manage multiple repositories.  If you don't have it
follow the instructions on this page to install the repo utility:

<http://source.android.com/source/downloading.html>

Now, initialize the environment and build your first policy: 

```
#!BASH
mkdir build-policies
cd build-policies
repo init -u https://bitbucket.org/quarksecurity/policies-manifest.git
repo sync
make -j

```

## Concepts
All of the variants start with the SE for Android's policy as a base, not AOSP's. 

The policy build system leverages inheritance to construct policies, so policy
variants build on top of SE for Android. Policy variants add additional functionality 
creating a policy to support a specific device with a specific set of features 
enabled.

## How do I use this thing?

By adding two things: a config file, and any policy components.


## How do I add my own policy git repo to the environment?  
Create a subdirectory
in variants/ and add the config files, described below, and any policy components.
Initialize it, add the files, then commit to it just like a normal git repo. Once you're ready to 
provide easy access to others create a local manifest repo containing an 
XML file.  Information on doing this can be found here:

<http://wiki.cyanogenmod.org/w/Doc:_Using_manifests>

### The config file
The config file is placed in `variants/<name>/<name>.mk`.  It must end in `.mk`.
Both `<name>` fields are arbitrary but by convention have some meaning
indicating what the config supports, e.g. `gs4-wifi`.

Here is an example configuration file with comments describing the various
fields in-line.

```
#!make
# You *always* set LOCAL_PATH, just like AOSP.
LOCAL_PATH := $(call my-dir)

# If this config will ultimately result in a policy variant being
# constructed, you must include START_POLICY at the beginning.
# Why wouldn't you want a policy variant constructed?  Consider
# core and base, it doesn't make sense to generate a policy using
# those components and only those components, as it wouldn't produce
# a functional device.
include $(START_POLICY)

# Include the SE for Android configuration.  Note tha ordering now
# becomes relevant.  SE for Android components can be overridden by base since it is
# included first.
include includes/aosp/aosp.mk

# Now you add your variant's policy components.
# This config file points to a policy directory containing a few possible
# subdirs, policy/replace, policy/union, policy/remove.
# Files in "replace" are copied *over* any files previously included.
# Files in "union" are concatenated with files previously included.
# Previously includes files in "remove" will be removed from the tree.
POLICY_DIRS := $(POLICY_DIRS) variants/example/example.mk

# Alphanumeric, required, used in tarball name
VARIANT_NAME := Example_Policy_from_SE_for_Android

# Used to indicate firmware target for this policy, optional, used in tarball name
#BUILD_NUMBER := JSS15J.I545VRUEMJ7

# Similar to START_POLICY above, if you're creating a variant
# you must call END_POLICY.  At a low-level, END_POLICY takes
# all of the information you provided above and generates
# make rules for building your policy variant.
include $(END_POLICY)

```


### Policy Files

Policy files are just that, a series of files that can either
be combined with existing policy files, a union, or replace existing, a
replace.  Refer to the configuration file above to see how you specify
directories used for union and replace.

For boolean files, us a bools_ prefix to specify your booleans.  This
makes it easy for others to manipulate booleans in a more fine-grained
manner.  For example, if we didn't use separate bool files, and cell 
and wifi booleans were in the same file, then a variant that tweaks those
booleans would have to modify the existing boolean file using sed. By
splitting them out into a bools_radio file, each variant can tweak the
two bools by adding their own bools_radio file to their replace dir.


### Includes
There is an includes/ directory containing policy components.  
These components do not create policies, but are inheritied by
variants to create policies.  They are quite simple, referencing
a policy directory similar to those above. 

Once important note, configuration files in includes/ *cannot*
set LOCAL_PATH.  It will cause breakage in the variants.


### Special Build Flags

To remove dontaudit rules from a specific policy, in a policy config,
after a START_POLICY add DONTAUDIT=y.

To disable validation of file contexts, seapp_contexts, and property_contexts,
after a START_POLICY add DISABLECHECKS=y.  This is useful for generating
policies with proprietary, vendor-specific formats and for leaving 
file context entries in-place that are found in the generated policy.
