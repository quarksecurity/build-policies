# Copyright (C) Quark Security Inc 2014

ifeq ($(QUIET),y)
hide := @ 
endif

START_POLICY := build/start_policy.mk
END_POLICY := build/end_policy.mk

outdir := $(CURDIR)/out

os := $(shell uname)
checkpol := build/prebuilts/$(os)/checkpolicy 
checkfc := build/prebuilts/$(os)/checkfc
checkseapp := build/prebuilts/$(os)/checkseapp
insertkeys := build/prebuilts/common/insertkeys.py 
insertkeys := build/prebuilts/common/insertkeys.py 

pol_files := security_classes initial_sids access_vectors global_macros mls_macros mls policy_capabilities te_macros attributes bools* *.te roles users initial_sid_contexts fs_use genfs_contexts port_contexts

include build/android.mk

$(foreach variant,$(shell find variants/* -regex .*\.mk),\
	$(eval DEPS := $(DEPS) $(variant))\
	$(eval include $(variant))\
	$(eval DEPS :=))

